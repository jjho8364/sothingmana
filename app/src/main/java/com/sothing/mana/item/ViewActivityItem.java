package com.sothing.mana.item;

public class ViewActivityItem {
    String imgUrl;

    public ViewActivityItem(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
