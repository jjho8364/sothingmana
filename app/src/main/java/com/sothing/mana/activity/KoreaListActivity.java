package com.sothing.mana.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.sothing.mana.R;
import com.sothing.mana.adapter.ListViewAdapter;
import com.sothing.mana.fragment.Fragment02;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class KoreaListActivity extends Activity implements View.OnClickListener {
    private String TAG = " KoreaListActivity - ";
    private ProgressDialog mProgressDialog;
    ListView listView;
    private GetListView getListView = null;
    private String baseUrl = "";

    private int adsCnt = 0;

    //// paging ////
    private int pageNum = 1;
    private TextView tv_currentPage;
    private TextView tv_lastPage;
    private Button nextBtn;
    private Button preBtn;
    private String lastPage = "1";

    private String listUrl = "";

    // history
    String[] sfArr = new String[5];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_korea_list);

        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        Intent intent = getIntent();
        baseUrl = intent.getStringExtra("listUrl");
        adsCnt = Integer.parseInt(intent.getStringExtra("adsCnt"));

        listView = (ListView)findViewById(R.id.listview);

        //// paging ////
        tv_currentPage = (TextView)findViewById(R.id.fr01_currentpage);
        tv_lastPage = (TextView)findViewById(R.id.fr01_lastpage);
        tv_currentPage.setText("0");
        tv_lastPage.setText("0");
        preBtn = (Button)findViewById(R.id.fr01_prebtn);
        nextBtn = (Button)findViewById(R.id.fr01_nextbtn);
        preBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);

        getListView = new GetListView();
        getListView.execute();
    }

    public class GetListView extends AsyncTask<Void, Void, Void> {
        ArrayList<String> btnTextArr = new ArrayList<String>();
        List<String> btnVideoUrlArr = new ArrayList<String>();

        ArrayList<String> listTitleArr = new ArrayList<String>();
        ArrayList<String> listPageUrlArr = new ArrayList<String>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(KoreaListActivity.this);
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;

            try {
                doc = Jsoup.connect(baseUrl + "&page=" + pageNum).timeout(15000).get();

                ////////////////// episode list //////////////
                Elements lists = doc.select(".table.list-pc tr");

                for(int i=1 ; i<lists.size() ; i++){
                    String title = lists.get(i).select(".list-subject a").text() + "    " + lists.get(i).select(".text-center.en").text().split(" ")[0];
                    String viewUrl = lists.get(i).select(".list-subject a").attr("href");
                    listTitleArr.add(title);
                    listPageUrlArr.add(viewUrl);
                }

                ////////////// get lat page /////////////
                if(lastPage.equals("1")){
                    Elements pageLists = doc.select(".pagination li a");
                    Log.d(TAG, "getUrl1 : " + pageLists.get(pageLists.size()-1).attr("href"));
                    String getUrl = pageLists.get(pageLists.size()-1).attr("href").replace("./", "/");
                    Log.d(TAG, "getUrl2 : " + getUrl);
                    if(getUrl != null && !getUrl.equals("")) {
                        String[] lastPageArr = getUrl.split("&page=");
                        lastPage = lastPageArr[lastPageArr.length-1];
                    }
                    Log.d(TAG, "lastPage : " + lastPage);
                }

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(KoreaListActivity.this != null){

                //// paging ////
                tv_currentPage.setText(pageNum+"");
                tv_lastPage.setText(lastPage);

                //////////// set listview ///////////
                //ArrayAdapter<String> adapter = new ArrayAdapter<String>(KoreaListActivity.this, android.R.layout.simple_list_item_1, listTitleArr);
                //listView.setAdapter(adapter);
                listView.setAdapter(new ListViewAdapter(KoreaListActivity.this, listTitleArr, listPageUrlArr, R.layout.listviewitem_list));

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        TextView tv = (TextView)view.findViewById(R.id.item_tv_list);
                        tv.setTextColor(getResources().getColor(R.color.purple_300));

                        Intent intent = new Intent(KoreaListActivity.this, ViewActivity.class);
                        intent.putExtra("listUrl", listPageUrlArr.get(position));
                        intent.putExtra("title", listTitleArr.get(position));
                        intent.putExtra("adsCnt", "0");
                        intent.putExtra("comicType", "2");
                        startActivity(intent);
                    }
                });

                mProgressDialog.dismiss();
            }

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fr01_prebtn :
                if(pageNum != 0){
                    pageNum--;
                    if(getListView != null){
                        getListView.cancel(true);
                    }
                    getListView = new GetListView();
                    getListView.execute();
                }
                break;

            case R.id.fr01_nextbtn :
                if(tv_lastPage.getText() != null && !(tv_lastPage.getText().toString().equals("")) && pageNum != Integer.parseInt(tv_lastPage.getText().toString())){
                    pageNum++;
                    if(getListView != null){
                        getListView.cancel(true);
                    }
                    getListView = new GetListView();
                    getListView.execute();
                }
                break;
        }
    }

}
